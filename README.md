# e-contact


# Présentation 
Ceci est un projet scolaire crée en ***PHP Symphony***. Nous devions réaliser un site d'article en 1 semaine.

# Requirement
- Composer [https://getcomposer.org/]
- PHP
- Symphony [https://symfony.com/download]

# Lancer le serveur local
Ouvrez un **terminal** sur le dossier puis executer la **commande** suivante :
`php bin/console server:run`

# Affichage des différentes page du site
**Page d'accueil** :
![](./image-readme/pageAccueil.png)

**Page de la liste d'article** :
![](./image-readme/pageListArticle.png)

**Page pour voir un article** :
![](./image-readme/watchArticle.png)

**Page de création d'article** :
![](./image-readme/pageCreateArticle.png)

